import pygame
import pygame.freetype
import random

FPS=60
WINDOW_SIZE_X=800
WINDOW_SIZE_Y=600
PLAYER_VEL=0.2
SPAWN_ENEMY_MAX_TIME=3000
SPAWN_ENEMY_MIN_TIME=1000
SPAWN_ENEMY_MAX_TIME_2=1500
SPAWN_ENEMY_MIN_TIME_2=800

# game states
GAME_STATE_MENU=1
GAME_STATE_PLAYING=2
GAME_STATE_RESULTS=3
GAME_STATE_EXIT=4


class RemovableObject:
  def __init__(self, originList, referencedObject):
    self.originList = originList
    self.referencedObject = referencedObject

def collisions(player, bullets, bulletsEnemies, enemies, powerups, explosions, sounds):
    sprite=player['sprite']
    player_collider=sprite.get_rect().move(player['x'], player['y'])

    stuffToDelete = []

    for i in range(len(powerups)-1, -1, -1):
        powerup = powerups[i]
        powerup_collider=powerup['sprite'].get_rect().move(powerup['x'], powerup['y'])
        
        if player_collider.colliderect(powerup_collider):
            collectPowerup(player, powerup, sounds)
            
            stuffToDelete.append(RemovableObject(powerups,powerup))

    for i in range(len(bulletsEnemies)-1, -1, -1):
        bulletEnemy = bulletsEnemies[i]
        bulletEnemy_collider=bulletEnemy['sprite'].get_rect().move(bulletEnemy['x'], bulletEnemy['y'])
        
        if player_collider.colliderect(bulletEnemy_collider):
            playerHurt(player, bulletEnemy['myParent'], explosions)
            
            stuffToDelete.append(RemovableObject(bulletsEnemies,bulletEnemy))


    for i in range(len(enemies)-1, -1, -1):
        enemy = enemies[i]
        enemy_collider=enemy['sprite'].get_rect().move(enemy['x'], enemy['y'])
        
        if player_collider.colliderect(enemy_collider):
            playerHurt(player, enemy, explosions)
            
            isEnemyDead = enemyHurt(enemy['hp'], player, enemy, powerups,explosions, True)

            if(isEnemyDead):
                stuffToDelete.append(RemovableObject(enemies,enemy))

        for j in range(len(bullets)-1, -1, -1):
            bullet = bullets[j]
            bullet_collider=bullet['sprite'].get_rect().move(bullet['x'], bullet['y'])

            if enemy_collider.colliderect(bullet_collider):
                
                stuffToDelete.append(RemovableObject(bullets,bullet))
                #stuffToDelete.append(RemovableObject(enemies,enemies[i]))

                isEnemyDead = enemyHurt(player['bulletDamage'], player, enemy, powerups,explosions,False,bullet['isUltimateBullet'])

                if(isEnemyDead):
                    stuffToDelete.append(RemovableObject(enemies,enemy))

    #borrar duplicados de la lista
    stuffToDelete = list(dict.fromkeys(stuffToDelete))

    for i in range(len(stuffToDelete)-1, -1, -1):

        if (isinstance(stuffToDelete[i], RemovableObject) and stuffToDelete[i].referencedObject in stuffToDelete[i].originList):
            stuffToDelete[i].originList.remove(stuffToDelete[i].referencedObject)

def playSound(soundName, sounds):
    finalSound=sounds[0]
    for sound in sounds:
        if(sound["name"]==soundName):
            finalSound=sound

    soundToPlay = pygame.mixer.Sound(finalSound["path"])
    soundToPlay.set_volume(finalSound["volume"])
    soundToPlay.play()

def getButton(buttonName, buttons):
    finalButton=buttons[0]
    for button in buttons:
        if(button["name"]==buttonName):
            finalButton=button

    return finalButton

def spawnExplosion(explosions, x, y):
    newExplosion={
        'sprite': pygame.transform.scale(pygame.image.load('images/effects/explosion1.png'), (60, 60)),
        'sprites': [
            #pygame.transform.scale(pygame.image.load('images/effects/explosion0.png'), (60, 60)),
            pygame.transform.scale(pygame.image.load('images/effects/explosion1.png'), (60, 60)),
            pygame.transform.scale(pygame.image.load('images/effects/explosion2.png'), (60, 60)),
            pygame.transform.scale(pygame.image.load('images/effects/explosion3.png'), (60, 60)),
            pygame.transform.scale(pygame.image.load('images/effects/explosion0.png'), (60, 60))
        ],
        'x': x,
        'y': y,
        'index':0,
        'animationTime':0
    }
    explosions.append(newExplosion)

def collectPowerup(player, powerup, sounds):
    player['powerupTime'] = powerup['powerupTime']*1000
    player['bulletDamage'] = powerup['bulletDamage']
    player['bulletsPerShot'] = powerup['bulletsPerShot']
    player['bulletSeparation'] = powerup['bulletSeparation']
    player['shoot_cooldown_max'] = powerup['bulletFireRate']
    player['bulletWidth'] = powerup['bulletWidth']
    player['bulletSpeed'] = powerup['bulletSpeed']
    player['isHomingBullet'] = powerup['isHomingBullet']

    playSound('collect', sounds)

def restorePlayerDefaults(player):
    player['bulletsPerShot'] = player['bulletsPerShotDefault']
    player['bulletDamage'] = player['bulletDamageDefault']
    player['bulletSeparation'] = player['bulletSeparationDefault']
    player['shoot_cooldown_max'] = player['shoot_cooldown_maxDefault']
    player['bulletWidth'] = player['bulletWidthDefault']
    player['bulletSpeed'] = player['bulletSpeedDefault']
    player['isHomingBullet'] = player['isHomingBulletDefault']


def playerHurt(player, enemy, explosions):
    finalDamage = enemy['points']*2
    if (player['points']-finalDamage < 0):
        player['points']=0
        spawnExplosion(explosions, player['x'], player['y'])
    else:
        player['points']-=finalDamage


def enemyHurt(damage, player, enemy, powerups, explosions, isPlayerColliding = False,isUltimateBullet = False):        
    enemy['hp']-=damage

    enemy['animationTime']=4
    enemy['sprite']=enemy['sprite_hurt']

    if(enemy['hp'] <= 0):
        if(isPlayerColliding == False):
            if(isUltimateBullet == False):
                player['points']+=enemy['points']
                player['killCount']+=1
                if(player['ultPercentage'] < 100):
                    player['ultPercentage']+=10
            powerupChance=random.randint(1, 5)
            if(powerupChance==1):
                powerups.append(spawn_powerup(create_powerup_types(), enemy))
        spawnExplosion(explosions, enemy['x'], enemy['y'])
        return True
    else:
        return False
                
            

def shoot_bullet(player, separation, sounds, isUltimateBullet = False):
    bullet={
        'sprite': pygame.transform.scale(pygame.image.load('images/bullet_0.png'), (50, player['bulletWidth'])),
        'speed': player['bulletSpeed'],
        'isUltimateBullet':isUltimateBullet,
        'x': player['x'],
        'y': player['y']+10+separation
    }
    playSound('shoot',sounds)
    return bullet

def shoot_bullet_enemy(enemy, separation):
    bullet={
        'sprite': pygame.transform.scale(pygame.image.load('images/bullet_1.png'), (50, enemy['bulletWidth'])),
        'speed': enemy['bulletSpeed'],
        'myParent': enemy,
        'x': enemy['x'],
        'y': enemy['y']+10+separation
    }
    return bullet

def spawn_powerup(powerup_types, enemy):
    powerup_type=random.choice(powerup_types)
    powerup={
        'sprite': pygame.transform.scale(powerup_type['sprite'], (30, 30)),
        'powerupTime': powerup_type['powerupTime'],
        'bulletDamage': powerup_type['bulletDamage'],
        'bulletsPerShot': powerup_type['bulletsPerShot'],
        'bulletSeparation':powerup_type['bulletSeparation'],
        'bulletFireRate':powerup_type['bulletFireRate'],
        'bulletWidth': powerup_type['bulletWidth'],
        'bulletSpeed': powerup_type['bulletSpeed'],
        'isHomingBullet':powerup_type['isHomingBullet'],
        'x': enemy['x'],
        'y': enemy['y']
    }
    return powerup


def spawn_enemy(enemy_types):
    enemy_type=random.choice(enemy_types)
    enemy={
        'name':enemy_type['name'],
        'sprite': pygame.transform.scale(enemy_type['sprite'], (50, 50)),
        'sprite_normal':pygame.transform.scale(enemy_type['sprite_normal'], (50, 50)),
        'sprite_hurt':pygame.transform.scale(enemy_type['sprite_hurt'], (50, 50)),
        'hp':enemy_type['hp'],
        'speed':enemy_type['speed'],
        'shot': False,
        'shoot_cooldown': 0,
        'shoot_cooldown_max': enemy_type['bulletFireRate'],
        'bulletsPerShot': enemy_type['bulletsPerShot'],
        'bulletSeparation':enemy_type['bulletSeparation'],
        'bulletWidth': enemy_type['bulletWidth'],
        'bulletSpeed': enemy_type['bulletSpeed'],
        'isHomingBullet':enemy_type['isHomingBullet'],
        'points': enemy_type['points'],
        'animationTime':0,
        'yDirection':1,
        'x': WINDOW_SIZE_X,
        'y': random.randint(0, WINDOW_SIZE_Y-enemy_type['sprite'].get_height())
    }
    return enemy

def move_bullet(bullets, bulletsEnemies, delta):
    for n in range(len(bullets)-1, -1, -1):
        bullet=bullets[n]
        vel=int(bullet['speed']*delta)
        bullet['x']+=vel

        if bullet['x']>=WINDOW_SIZE_X-10:
            del bullets[n]

    for n in range(len(bulletsEnemies)-1, -1, -1):
        bullet=bulletsEnemies[n]
        vel=int(bullet['speed']*-1*delta)
        bullet['x']+=vel

        if bullet['x']>=WINDOW_SIZE_X-10:
            del bulletsEnemies[n]

def move_enemy(enemies, bulletsEnemies, delta):
    for n in range(len(enemies)-1, -1, -1):
        enemy=enemies[n]
        vel=int(enemy['speed']/10*delta)
        if(enemy['name']=='enemy2'):
            
            enemy['y']+=(vel+2)*enemy['yDirection']

            if(enemy['y']>WINDOW_SIZE_Y-50):
                enemy['yDirection']=-1
            if(enemy['y']<=0):
                enemy['yDirection']=1
        
        enemy['x']-=vel

        if(enemy['animationTime'] >0):
            enemy['animationTime']-=1
        else:
            enemy['sprite']=enemy['sprite_normal']



        if enemy['shot'] == False:
            enemy['shot'] = True
            if(enemy['bulletsPerShot']%2 == 0):
                for index in range(enemy['bulletsPerShot']+1):
                    isNegative = -1
                    finalBulletSeparation = enemy['bulletSeparation']
                    if(index%2 == 0 and index != 0):
                        isNegative=1
                        index=index-1

                    if(index != 0):
                        bulletsEnemies.append(shoot_bullet_enemy(enemy, finalBulletSeparation*index*isNegative))
                    
            else:
                for index in range(enemy['bulletsPerShot']):
                    isNegative = -1
                    finalBulletSeparation = enemy['bulletSeparation']
                    if(index%2 == 0 and index != 0):
                        isNegative=1
                        index=index-1

                    bulletsEnemies.append(shoot_bullet_enemy(enemy, finalBulletSeparation*index*isNegative))

        if enemy['shot'] == True:
            enemy['shoot_cooldown']+=delta
            if enemy['shoot_cooldown']>enemy['shoot_cooldown_max']:
                enemy['shot'] =  False
                enemy['shoot_cooldown']=0
        
        if enemy['x']<=0:
            del enemies[n]

def move_powerup(powerups, delta):
    vel=int(0.1*delta)
    for n in range(len(powerups)-1, -1, -1):
        powerup=powerups[n]
        powerup['x']-=vel

        if powerup['x']<=0:
            del powerups[n]

def move_explosion(explosions, delta):
    for n in range(len(explosions)-1, -1, -1):
        explosion=explosions[n]
        explosion['sprite']=explosion['sprites'][explosion['index']]
        explosion['animationTime']+=delta
        if explosion['animationTime']>80:
            explosion['animationTime']=0
            if explosion['index']>=len(explosion['sprites'])-1:
                del explosions[n]
            else:
                explosion['index']+=1

def move_player(player, delta, bullets, sounds):
    if(player['points'] > 0):
        vel=int(PLAYER_VEL*delta)
        keys = pygame.key.get_pressed()

        player['sprite']=player['default']

        if keys[pygame.K_LEFT] and player['x']>0:
            player['x']=max(player['x']-vel, 0)
            player['sprite']=player['default']
        if keys[pygame.K_RIGHT] and player['x']<WINDOW_SIZE_X-25:
            player['x']=min(player['x']+vel, WINDOW_SIZE_X-25)
            player['sprite']=player['default']
        if keys[pygame.K_UP] and player['y']>0:
            player['y']=max(player['y']-vel, 0)
            player['sprite']=player['up']
        if keys[pygame.K_DOWN] and player['y']<WINDOW_SIZE_Y-25:
            player['y']=min(player['y']+vel, WINDOW_SIZE_Y-25)
            player['sprite']=player['down']
        if(keys[pygame.K_x] and player['ultPercentage'] == 100): 
            auxDamage = player['bulletDamage']
            player['bulletDamage'] = 1000
            numBullets = 1000    
        
            for index in range(numBullets):
                isNegative = -1
                finalBulletSeparation = 2
                if(index%2 == 0 and index != 0):
                    isNegative=1
                    index=index-1

                if(index != 0):
                    bullets.append(shoot_bullet(player, finalBulletSeparation*index*isNegative, sounds,True))
            
            player['bulletDamage'] = auxDamage
            player['ultPercentage'] = 0

        '''if keys[pygame.K_z]: 
            if(player['isShooting']):
                player['isShooting']=False
            else:
                player['isShooting']=True'''
        
        #controles de Z y X estan en los eventos del main
  
        if player['isShooting'] and player['shot'] == False:
            player['shot'] = True
            if(player['bulletsPerShot']%2 == 0):
                for index in range(player['bulletsPerShot']+1):
                    isNegative = -1
                    finalBulletSeparation = player['bulletSeparation']
                    if(index%2 == 0 and index != 0):
                        isNegative=1
                        index=index-1

                    if(index != 0):
                        bullets.append(shoot_bullet(player, finalBulletSeparation*index*isNegative, sounds))
                    
            else:
                for index in range(player['bulletsPerShot']):
                    isNegative = -1
                    finalBulletSeparation = player['bulletSeparation']
                    if(index%2 == 0 and index != 0):
                        isNegative=1
                        index=index-1

                    '''if(index < 2):
                        #finalBulletSeparation = player['bulletSeparation']*2
                    else:
                        finalBulletSeparation = player['bulletSeparation']'''
                    #hacer que se haga otra vez index 
                    bullets.append(shoot_bullet(player, finalBulletSeparation*index*isNegative, sounds))

           

        if player['shot'] == True:
            player['shoot_cooldown']+=delta
            if player['shoot_cooldown']>player['shoot_cooldown_max']:
                player['shot'] =  False
                player['shoot_cooldown']=0
        
        if(player['powerupTime'] > 0):
            if(player['powerupTime']-delta < 0):
                player['powerupTime']=0
            else:
                player['powerupTime']-=delta

            if(player['powerupTime'] == 0):
                restorePlayerDefaults(player)
                


def draw(screen, font, player, bullets, bulletsEnemies, enemies, powerups, explosions, button, level):
    
    background = pygame.image.load(level['background'])
    screen.blit(background, background.get_rect().move(0, 0))

    for enemy in enemies:
        sprite=enemy['sprite']
        square=sprite.get_rect().move(enemy['x'], enemy['y'])
        screen.blit(sprite, square)

    for bullet in bulletsEnemies:
        sprite=bullet['sprite']
        square=sprite.get_rect().move(bullet['x'], bullet['y'])
        screen.blit(sprite, square)

    for bullet in bullets:
        sprite=bullet['sprite']
        square=sprite.get_rect().move(bullet['x'], bullet['y'])
        screen.blit(sprite, square)

    for powerup in powerups:
        sprite=powerup['sprite']
        square=sprite.get_rect().move(powerup['x'], powerup['y'])
        screen.blit(sprite, square)

    for explosion in explosions:
        sprite=explosion['sprite']
        square=sprite.get_rect().move(explosion['x'], explosion['y'])
        screen.blit(sprite, square)

    if(player['points'] == 0):
        font.render_to(screen, (300, 250), 'GAME OVER', (255,255,255))
    else:
        sprite=player['sprite']
        square=sprite.get_rect().move(player['x'], player['y'])
        screen.blit(sprite, square)
    if(player['killCount'] >= player['killCountMax'] and level['number'] == 1):
        font.render_to(screen, (300, 250), 'VICTORY!', (255,255,255))
        font.render_to(screen, (300, 300), 'Puntuació: '+str(player['points']), (255,255,255))

    if((player['killCount'] >= player['killCountMax'] and level['number'] == 1) or player['points']==0):
        square=button["sprite"].get_rect().move(button["x"], button["y"])
        if square.collidepoint(pygame.mouse.get_pos()):
            button["sprite"]=button["hoverSprite"]
        else:
            button["sprite"]=button["normalSprite"]
        screen.blit(button["sprite"], button["sprite"].get_rect().move(button["x"],button["y"]))
    else:
        if(player['ultPercentage'] == 100):
            font.render_to(screen, (600, 500), 'Press [X]!!!', (255,255,255))
        font.render_to(screen, (550, 550), 'Ultimate: '+str(player['ultPercentage'])+'%', (255,255,255))
        font.render_to(screen, (50, 550), 'Puntuació: '+str(player['points']), (255,255,255))


def create_enemy_types():
    enemy_types=[
        {'name':'enemy0',
         'sprite': pygame.image.load('images/enemies/enemy_0.png'),
         'sprite_normal':pygame.image.load('images/enemies/enemy_0.png'),
         'sprite_hurt':pygame.image.load('images/enemies/enemy_0_hurt.png'),
         'hp': 10,
         'speed': 1,
         'bulletsPerShot': 1,
         'bulletSeparation': 10,
         'bulletFireRate': 2000,
         'bulletWidth': 20,
         'bulletSpeed': 0.2,
         'isHomingBullet': False,
         'points': 10},
        {'name':'enemy1',
         'sprite': pygame.image.load('images/enemies/enemy_1.png'),
         'sprite_normal':pygame.image.load('images/enemies/enemy_1.png'),
         'sprite_hurt':pygame.image.load('images/enemies/enemy_1_hurt.png'),
         'hp': 10,
         'speed': 1,
         'bulletsPerShot': 3,
         'bulletSeparation': 50,
         'bulletFireRate': 2500,
         'bulletWidth': 20,
         'bulletSpeed': 0.2,
         'isHomingBullet': False,
         'points': 8},
        {'name':'enemy2',
         'sprite': pygame.image.load('images/enemies/enemy_2.png'),
         'sprite_normal':pygame.image.load('images/enemies/enemy_2.png'),
         'sprite_hurt':pygame.image.load('images/enemies/enemy_2_hurt.png'),
         'hp': 10,
         'speed': 0.8,
         'bulletsPerShot': 2,
         'bulletSeparation': 30,
         'bulletFireRate': 2300,
         'bulletWidth': 20,
         'bulletSpeed': 0.2,
         'isHomingBullet': False,
         'points': 12}
    ]
    return enemy_types

def create_powerup_types():
    powerup_types=[
        {'sprite': pygame.image.load('images/fruits/fruit1.png'),
         'powerupTime':5,
         'bulletDamage':5,
         'bulletsPerShot': 3,
         'bulletSeparation':30,
         'bulletFireRate':1000,
         'bulletWidth': 25,
         'bulletSpeed':0.5,
         'isHomingBullet':False},
         {'sprite': pygame.image.load('images/fruits/fruit2.png'),
         'powerupTime':5,
         'bulletDamage':1,
         'bulletsPerShot': 5,
         'bulletSeparation':30,
         'bulletFireRate':50,
         'bulletWidth': 15,
         'bulletSpeed':0.75,
         'isHomingBullet':False}
    ]
    return powerup_types

def create_player():
    player={
        'default': pygame.image.load('images/player/player_0.png'),
        'up': pygame.image.load('images/player/player_up.png'),
        'down': pygame.image.load('images/player/player_down.png'),
        'sprite': pygame.image.load('images/player/player_0.png'),
        'x':400-27,
        'y':300-15,
        'points':1,
        'ultPercentage':0,
        'shot':False,
        'isShooting':False,
        'shoot_cooldown': 0,
        'shoot_cooldown_max': 100,
        'powerupTime': 0,
        'bulletDamage':1,
        'bulletsPerShot': 1,
        'bulletSeparation': 0,
        'bulletWidth': 10,
        'bulletSpeed': 0.9,
        'isHomingBullet':False,
        'bulletDamageDefault':1,
        'shoot_cooldown_maxDefault': 100,
        'bulletsPerShotDefault': 1,
        'bulletSeparationDefault': 0,
        'bulletWidthDefault': 10,
        'bulletSpeedDefault': 0.9,
        'isHomingBulletDefault':False,
        'killCount':0,
        'killCountMax':30
    }
    return player

def enemySpawner(enemySpawnerCooldown, enemySpawnTime,enemies, enemy_types):
    if enemySpawnerCooldown>enemySpawnTime:
        enemies.append(spawn_enemy(enemy_types))
        return True
    else:
        return False


def level_playing(screen, player, level, volume):
    overlay_font = pygame.freetype.Font("fonts/Lato-Black.ttf", 32)
    
    #create_powerup_types no esta aqui
    bullets=[]
    bulletsEnemies=[]
    enemies=[]
    powerups=[]
    explosions=[]
    globalVolume=volume
    sounds = [
        {
            'name':'shoot',
            'path':'sounds/alienshoot2.wav',
            'volume':globalVolume
        },
        {
            'name':'collect',
            'path':'sounds/coin.wav',
            'volume':globalVolume
        }
    ]
    exit_btn={
        'name':'exit',
        'sprite':pygame.transform.scale(pygame.image.load('images/menu/crossB1.png'),(100, 100)),
        'normalSprite':pygame.transform.scale(pygame.image.load('images/menu/crossB1.png'),(100, 100)),
        'hoverSprite':pygame.transform.scale(pygame.image.load('images/menu/crossB2.png'),(100, 100)),
        'optionsButton':False,
        'x':350,
        'y':450
    }

    gameOver=False

    clock = pygame.time.Clock()
    enemySpawnerCooldown=0
    enemySpawnTime = level["spawnMin"]
    going=True
    while going:
        delta=clock.tick(FPS)
        if(player['points']==0 or player['killCount'] >= player['killCountMax']):
            gameOver=True
            if(level['number'] != 1 and player['points']>0):
                #mucho cuidao con este if y esta linea si se añaden mas niveles
                result=0
                going=False
        
        enemySpawnerCooldown+=delta
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                going=False
                result=GAME_STATE_EXIT
            if event.type == pygame.MOUSEBUTTONDOWN:
                if(gameOver):
                    square=exit_btn["sprite"].get_rect().move(exit_btn["x"], exit_btn["y"])
                    if square.collidepoint(pygame.mouse.get_pos()):
                        going=False
                        result=GAME_STATE_MENU
            #este control lo tengo que poner aqui porque no quiero que sea uno de mantener, sino un toggle, y parece que los eventos solo los puedo mirar en el main
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_z:
                    if(player['isShooting']):
                        player['isShooting']=False
                    else:
                        player['isShooting']=True

        screen.fill((128,128,128))

        if(gameOver == False):
            hasEnemySpawned = enemySpawner(enemySpawnerCooldown, enemySpawnTime, enemies, level['enemyTypes'])
            if hasEnemySpawned:
                enemySpawnerCooldown=0
                enemySpawnTime=random.randint(level['spawnMin'], level['spawnMax'])
            move_player(player, delta, bullets, sounds)
            move_bullet(bullets, bulletsEnemies, delta)
            move_enemy(enemies, bulletsEnemies, delta)
            move_powerup(powerups, delta)
            collisions(player, bullets, bulletsEnemies, enemies, powerups, explosions, sounds)
        move_explosion(explosions, delta)
        draw(screen, overlay_font, player, bullets, bulletsEnemies,enemies, powerups, explosions, exit_btn, level)
        pygame.display.flip()
    return result 

def game_playing(screen, volume):
    player=create_player()
    levels=[
        {
            'number':0,
            'enemyTypes':create_enemy_types(),
            'background':'images/background_0.png',
            'spawnMin':SPAWN_ENEMY_MIN_TIME,
            'spawnMax':SPAWN_ENEMY_MAX_TIME
        },
        {
            'number':1,
            'enemyTypes':create_enemy_types(),
            'background':'images/background_1.png',
            'spawnMin':SPAWN_ENEMY_MIN_TIME_2,
            'spawnMax':SPAWN_ENEMY_MAX_TIME_2
        }
    ]

    result = level_playing(screen, player, levels[0], volume)
    if(player['killCount']>=player['killCountMax']):
        player['killCount']=0
        result = level_playing(screen, player, levels[1], volume)
       
    return result

def game_menu(screen):
    title=pygame.transform.scale(pygame.image.load('images/menu/title.png'), (700, 200))

    buttons = [
        {
            'name':'start',
            'sprite':pygame.transform.scale(pygame.image.load('images/menu/play1.png'),(100, 100)),
            'normalSprite':pygame.transform.scale(pygame.image.load('images/menu/play1.png'),(100, 100)),
            'hoverSprite':pygame.transform.scale(pygame.image.load('images/menu/play2.png'),(100, 100)),
            'optionsButton':False,
            'x':50,
            'y':500
        },
        {
            'name':'options',
            'sprite':pygame.transform.scale(pygame.image.load('images/menu/volume1.png'),(100, 100)),
            'normalSprite':pygame.transform.scale(pygame.image.load('images/menu/volume1.png'),(100, 100)),
            'hoverSprite':pygame.transform.scale(pygame.image.load('images/menu/volume2.png'),(100, 100)),
            'optionsButton':False,
            'x':300,
            'y':500
        },
        {
            'name':'options2',
            'sprite':pygame.transform.scale(pygame.image.load('images/menu/crossY1.png'),(70, 70)),
            'normalSprite':pygame.transform.scale(pygame.image.load('images/menu/crossY1.png'),(70, 70)),
            'hoverSprite':pygame.transform.scale(pygame.image.load('images/menu/crossY2.png'),(70, 70)),
            'optionsButton':True,
            'x':350,
            'y':400
        },
        {
            'name':'exit',
            'sprite':pygame.transform.scale(pygame.image.load('images/menu/crossB1.png'),(100, 100)),
            'normalSprite':pygame.transform.scale(pygame.image.load('images/menu/crossB1.png'),(100, 100)),
            'hoverSprite':pygame.transform.scale(pygame.image.load('images/menu/crossB2.png'),(100, 100)),
            'optionsButton':False,
            'x':550,
            'y':500
        },
        {
            'name':'volumeUp',
            'sprite':pygame.transform.scale(pygame.image.load('images/menu/plus1.png'),(70, 70)),
            'normalSprite':pygame.transform.scale(pygame.image.load('images/menu/plus1.png'),(70, 70)),
            'hoverSprite':pygame.transform.scale(pygame.image.load('images/menu/plus2.png'),(70, 70)),
            'optionsButton':True,
            'x':300,
            'y':300
        },
        {
            'name':'volumeDown',
            'sprite':pygame.transform.scale(pygame.image.load('images/menu/minus1.png'),(70, 70)),
            'normalSprite':pygame.transform.scale(pygame.image.load('images/menu/minus1.png'),(70, 70)),
            'hoverSprite':pygame.transform.scale(pygame.image.load('images/menu/minus2.png'),(70, 70)),
            'optionsButton':True,
            'x':400,
            'y':300
        }
    ]
    
    start_btn=getButton('start', buttons)
    options_btn=getButton('options', buttons)
    options_btn2=getButton('options2', buttons)
    exit_btn=getButton('exit', buttons)
    volumeUp_btn=getButton('volumeUp', buttons)
    volumeDown_btn=getButton('volumeDown', buttons)
    background=pygame.image.load('images/menu/background.png')
    volume=1
    openOptions=False
    going=True
    while going:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                going=False
                result=GAME_STATE_EXIT
            if event.type == pygame.MOUSEBUTTONDOWN:
                square=start_btn["sprite"].get_rect().move(start_btn["x"], start_btn["y"])
                if square.collidepoint(pygame.mouse.get_pos()):
                    going=False
                    result={
                        'state':GAME_STATE_PLAYING,
                        'volume':volume
                    }
                square=options_btn["sprite"].get_rect().move(options_btn["x"], options_btn["y"])
                if square.collidepoint(pygame.mouse.get_pos()):
                    if(openOptions):
                        openOptions=False
                    else:
                        openOptions=True
                square=exit_btn["sprite"].get_rect().move(exit_btn["x"], exit_btn["y"])
                if square.collidepoint(pygame.mouse.get_pos()):
                    going=False
                    result={
                        'state':GAME_STATE_EXIT,
                        'volume':volume
                    }
                if(openOptions):
                    square=options_btn2["sprite"].get_rect().move(options_btn2["x"], options_btn2["y"])
                    if square.collidepoint(pygame.mouse.get_pos()):
                        if(openOptions):
                            openOptions=False
                        else:
                            openOptions=True
                    square=volumeUp_btn["sprite"].get_rect().move(volumeUp_btn["x"], volumeUp_btn["y"])
                    if square.collidepoint(pygame.mouse.get_pos()):
                        if(volume < 1):
                            volume+=0.1
                            volume=round(volume, 1)
                    square=volumeDown_btn["sprite"].get_rect().move(volumeDown_btn["x"], volumeDown_btn["y"])
                    if square.collidepoint(pygame.mouse.get_pos()):
                        if(volume > 0):
                            volume-=0.1
                            volume=round(volume, 1)

        screen.blit(background, background.get_rect())
        screen.blit(title, title.get_rect().move(400-352, 50))
        if(openOptions):
            menuFondo = pygame.transform.scale(pygame.image.load('images/menu/background.png'), (300, 400))
            screen.blit(menuFondo, menuFondo.get_rect().move(250, 100))
            pygame.freetype.Font("fonts/Lato-Black.ttf", 32).render_to(screen, (300, 250), 'Volume: '+str(volume*100)+'%', (255,255,255))

        for button in buttons:
            square=button["sprite"].get_rect().move(button["x"], button["y"])
            if square.collidepoint(pygame.mouse.get_pos()):
                button["sprite"]=button["hoverSprite"]
            else:
                button["sprite"]=button["normalSprite"]

            if(button["optionsButton"]):
                if(openOptions):
                    screen.blit(button["sprite"], button["sprite"].get_rect().move(button["x"],button["y"]))
            else:
                screen.blit(button["sprite"], button["sprite"].get_rect().move(button["x"],button["y"]))
        '''square=options_btn.get_rect().move(300, 500)
        if square.collidepoint(pygame.mouse.get_pos()):
            options_btn=options_btn_dark
        else:
            options_btn=options_btn_light
        square=exit_btn.get_rect().move(450, 500)
        if square.collidepoint(pygame.mouse.get_pos()):
            exit_btn=exit_btn_dark
        else:
            exit_btn=exit_btn_light
        screen.blit(background, background.get_rect())
        screen.blit(title, title.get_rect().move(400-352, 50))
        screen.blit(start_btn, start_btn.get_rect().move(150,500))
        screen.blit(options_btn, options_btn.get_rect().move(300,500))
        screen.blit(exit_btn, exit_btn.get_rect().move(450,500))
        screen.blit(start_btn["sprite"], start_btn["sprite"].get_rect().move(start_btn["x"],start_btn["y"]))'''
        
        
        pygame.display.flip()
    return result


def main():
    pygame.init()
    screen = pygame.display.set_mode([WINDOW_SIZE_X, WINDOW_SIZE_Y])
    game_icon = pygame.image.load('images/icon.png')
    volume=1
    pygame.display.set_caption('Proyecto Nave')
    pygame.display.set_icon(game_icon)

    pygame.mixer.init()

    game_state=GAME_STATE_MENU
    while game_state!=GAME_STATE_EXIT:
        if game_state==GAME_STATE_MENU:
            results=game_menu(screen)
            volume=results["volume"]
            game_state=results["state"]
        elif game_state==GAME_STATE_PLAYING:
            game_state=game_playing(screen, volume)
    pygame.quit()

main()
