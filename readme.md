NAVE PYGAME - Sergio Monfort

CONTROLS

 - Fletxes per moure's. 
 - Z per activar/desactivar els dispars.
 - X per llençar l'habilitat definitiva un cop estigui carregada.

DESCRIPCIÓ

 - El joc consisteix en una nau que es pot moure en qualsevol direcció per la pantalla, pots activar i desactivar el mode disparar, aconseguir punts per carregar una 	habilitat definitiva, aconseguir punts de vida al matar enemics, i agafar powerups que et donen habilitats especials durant un temps limitat.
 - Els enemics van apareixent per la dreta de la pantalla, cadascun amb atacs diferents, al matar-los et donen punts de vida, però si et toquen et resten punts de vida, i si et quedes sense, perds. 
 - Al matar un nombre concret d'enemics, es passa al següent nivell.

AESTHETICS 

 - el joc està basat en naus espacials, i l'espai en general (semblant a star wars)

ELEMENTS

- jugador
- enemics
- projectils
- powerups	

INTERACCIONS

 - El jugador pot disparar els enemics, els enemics poden tocar i disparar el jugador.
 - El jugador al tocar els powerups els consumeix i activa.
 - Els enemics no poden agafar powerups.
 - Els powerups es mouen cap a l'esquerra i desapareixen si arriben al final sense que el jugador els agafi.
 - Els powerups s'activen inmediatament al ser agafats i duren uns segons.
 - Les bales (projectils) poden ser del jugador o enemigues, les enemigues només afectaran al jugador, i viceversa.

RESTRICCIONS DEL PROJECTE

 - [X] Controls simples, ja sigui clicar un o dos botons.
 - [X] El jugador ha de poder moure la seva nau per la pantalla.
 - [X] La nau ha de poder disparar.
 - [X] Han d’aparèixer enemics.
 - [X] Els enemics han de poder destruir la nau del jugador d’alguna forma.
 - [X] El jugador ha de poder disparar i eliminar als enemics.
 - [X] Hi ha d’haver algun tipus d’informació sobre el joc a pantalla: vides, energia, puntuació… (PUNTS DE VIDA I HABILITAT DEFINITIVA)
 - [X] Hi ha d’haver diverses pantalles completament operatives. Les pantalles han de ser clarament diferents.
 - [X] Hi ha d’haver diversos tipus de powerups que el jugador ha de poder agafar.
 - [X] Hi ha d’haver diversos tipus d’armes que el jugador pot utilitzar (opcionalment associades als powerups).
 - [X] Hi ha d’haver diversos tipus d’enemics que es comportin de manera diferent (diferents recorreguts, diferents armes…).
 - [X] El joc ha de tenir un objectiu i ha d’acabar quan aquest es compleix. (AL MATAR SUFICIENTS ENEMICS ES GUANYA)
 - [X] El jugador ha de poder perdre (nombre de vides o energia limitada…). El joc ha d’acabar almenys amb un missatge de final de joc, tant si guanyes com si perds.
 - [X] El joc ha de tenir so (música, efectes per les armes…). El jugador ha de poder regular el volum del so i l’ha de poder silenciar.
 - [X] Hi ha d’haver alguna animació (per exemple, efecte d’explosió quan es destrueix una nau…)
 - [X] El joc ha d’estar ben acabat. És preferible que el joc sigui curt però ben fet, que no llarg, però hi hagi coses a mitges.
 - [X] El joc ha de tenir una pantalla de presentació amb, com a mínim, l’opció de jugar, l’opció de sortir, i les opcions de configuració que es considerin.

FUNCIONALITATS ADDICIONALS

 - [X] recurs d'habilitat definitiva al matar suficients enemics
 - [X] feedback al fer mal als enemics (es posen de color vermell per indicar que els hi ha afectat la teva bala)

